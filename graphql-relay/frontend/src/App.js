import React from 'react';
import './App.css';
import graphql from 'babel-plugin-relay/macro';
import { loadQuery, RelayEnvironmentProvider, useFragment, usePreloadedQuery } from 'react-relay/hooks';
import RelayEnvironment from './RelayEnvironment';
import { ErrorBoundary } from 'react-error-boundary';

const { Suspense } = React;

const SingleNodeQuery = graphql`
  query AppSingleNodeQuery {
    user: node(id: 4294967297) {
      ...on User {
        name
      }
      ...AppUserID
    }
  }
`;

const preloadedQuery = loadQuery(RelayEnvironment, SingleNodeQuery, {});

function App(props) {
  const data = usePreloadedQuery(SingleNodeQuery, props.preloadedQuery);

  return (<div>
    <strong>Name:</strong> {data.user.name}<br/>
    {/*<MoreDetails user={data.user}/>*/}
  </div>);
}

function MoreDetails(props) {
  const user = useFragment(
    graphql`
      fragment AppUserID on User {
        id
      }
    `,
    props.user,
  );

  console.log(props);
  return (<div>
    <strong>ID:</strong> {user?.id}<br/>
  </div>);
}

function AppRoot(props) {
  return (
    <RelayEnvironmentProvider environment={RelayEnvironment}>
      <ErrorBoundary fallbackRender={({ error }) => <div>{error.message}</div>}>
        <Suspense fallback={'Loading...'}>
          <App preloadedQuery={preloadedQuery}/>
        </Suspense>
      </ErrorBoundary>
    </RelayEnvironmentProvider>
  );
}

export default AppRoot;
