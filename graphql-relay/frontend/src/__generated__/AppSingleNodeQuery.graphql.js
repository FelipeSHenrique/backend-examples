/**
 * @generated SignedSource<<141b3308f0bd1667a41c48a044864f8d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

var node = (function(){
var v0 = [
  {
    "kind": "Literal",
    "name": "id",
    "value": 4294967297
  }
],
v1 = {
  "kind": "InlineFragment",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "AppSingleNodeQuery",
    "selections": [
      {
        "alias": "user",
        "args": (v0/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          (v1/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "AppUserID"
          }
        ],
        "storageKey": "node(id:4294967297)"
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "AppSingleNodeQuery",
    "selections": [
      {
        "alias": "user",
        "args": (v0/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          (v1/*: any*/)
        ],
        "storageKey": "node(id:4294967297)"
      }
    ]
  },
  "params": {
    "cacheID": "f6a966aee59393ad9aa4f37bf97253ca",
    "id": null,
    "metadata": {},
    "name": "AppSingleNodeQuery",
    "operationKind": "query",
    "text": "query AppSingleNodeQuery {\n  user: node(id: 4294967297) {\n    __typename\n    ... on User {\n      name\n    }\n    ...AppUserID\n    id\n  }\n}\n\nfragment AppUserID on User {\n  id\n}\n"
  }
};
})();

node.hash = "05ac84c37011cd9e834c7756a1e59c60";

module.exports = node;
