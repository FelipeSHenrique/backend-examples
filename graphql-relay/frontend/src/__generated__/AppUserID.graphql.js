/**
 * @generated SignedSource<<9b66f3524ca43b0aabebaa8388ce9587>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* eslint-disable */

'use strict';

var node = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "AppUserID",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

node.hash = "fc65cf683a144764eb89da72c0cbbe88";

module.exports = node;
