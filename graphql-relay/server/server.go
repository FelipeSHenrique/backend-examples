package main

import (
	"context"
	"log"
	"net/http"

	"entgo.io/bug"
	"entgo.io/bug/ent"
	"entgo.io/bug/ent/migrate"
	_ "entgo.io/bug/ent/runtime"
	"entgo.io/contrib/entgql"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	client, err := ent.Open(
		"sqlite3",
		"./db.sqlite?cache=shared&_fk=1",
	)
	if err != nil {
		log.Fatalf("opening ent client: %v", err)
	}
	if err := client.Schema.Create(
		context.Background(),
		migrate.WithGlobalUniqueID(true),
	); err != nil {
		log.Fatalf("running schema migration %v", err)
	}

	srv := handler.NewDefaultServer(bug.NewSchema(client))
	srv.Use(entgql.Transactioner{TxOpener: client})

	http.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		playground.Handler("Todo", "/query").ServeHTTP(w, r)
	}))

	http.Handle("/query", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		srv.ServeHTTP(w, r)
	}))

	if err := http.ListenAndServe(":8822", nil); err != nil {
		log.Fatalf("http server terminated: %v", err)
	}
}
